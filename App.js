/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

// CONTEXT
import MovieState from './src/context/movies/movieState';

// SCRENS
import HomeScreen from './src/screens/HomeScreen';
import MovieDetailsScreen from './src/screens/MovieDetailsScreen';

import {colors} from './src/theme';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS,
        headerTitleAlign: 'center',
        headerTintColor: colors.white, // back arrow color
        headerTitleStyle: {
          //color: colors.white,
        },
        headerStyle: {
          height: 50,
          backgroundColor: colors.main,
        },
      }}>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerTitle: () => (
            <Image
              source={require('./src/images/tmdb-logo.png')}
              style={{width: 130, height: 19}}
            />
          ),
        }}
      />
      <Stack.Screen
        name="Movie Details"
        component={MovieDetailsScreen}
        options={{
          headerTitle: () => (
            <Image
              source={require('./src/images/tmdb-logo.png')}
              style={{width: 130, height: 19}}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <MovieState>
      <NavigationContainer>
        <HomeStack></HomeStack>
      </NavigationContainer>
    </MovieState>
  );
};

const styles = StyleSheet.create({});

export default App;
