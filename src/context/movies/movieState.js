import React, {useReducer, useEffect} from 'react';
import tmdbApi from '../../api/tmdbApi';

import MovieContext from './movieContext';
import MovieReducer from './movieReducer';

import {API_KEY} from '@env';

import {
  GET_POPULAR_MOVIES,
  GET_MOVIE_BY_ID,
  GET_MOVIE_CREDITS_BY_ID,
  ACTIVATE_SEARCH,
  GET_SEARCH_RESULTS,
  CLEAR_SEARCH_RESULTS,
} from '../types';

const MovieState = props => {
  const initialState = {
    movies: [],
    movie: {},
    movie_credits: [],
    searchStatus: false,
    searchResults: [],
  };

  const [state, dispatch] = useReducer(MovieReducer, initialState);

  // Get Popular Movies
  const getPopularMovies = async () => {
    try {
      const response = await tmdbApi.get(`/movie/popular?api_key=${API_KEY}`);
      dispatch({
        type: GET_POPULAR_MOVIES,
        payload: response.data.results,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getPopularMovies(); // On first render get popular movies
  }, []);

  // Get Movie By ID
  const getMovieById = async id => {
    try {
      const response = await tmdbApi.get(`/movie/${id}?api_key=${API_KEY}`);
      dispatch({
        type: GET_MOVIE_BY_ID,
        payload: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  // Get Movie Credits By ID
  const getMovieCreditsById = async id => {
    try {
      const response = await tmdbApi.get(
        `/movie/${id}/credits?api_key=${API_KEY}`,
      );
      dispatch({
        type: GET_MOVIE_CREDITS_BY_ID,
        payload: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  // Activate Search Bar
  const changeSearchStatus = status =>
    dispatch({type: ACTIVATE_SEARCH, payload: status});

  // Search Movies
  const searchMovies = async term => {
    if (term) {
      try {
        const response = await tmdbApi.get(
          `/search/movie?api_key=${API_KEY}&query=${term}`,
        );
        dispatch({
          type: GET_SEARCH_RESULTS,
          payload: response.data.results,
        });
      } catch (error) {
        console.log(error);
      }
    }
  };

  // Clear Search results
  const clearSearchResults = () => dispatch({type: CLEAR_SEARCH_RESULTS});

  return (
    <MovieContext.Provider
      value={{
        movies: state.movies,
        movie: state.movie,
        movie_credits: state.movie_credits,
        searchStatus: state.searchStatus,
        searchResults: state.searchResults,
        getPopularMovies,
        getMovieById,
        getMovieCreditsById,
        changeSearchStatus,
        searchMovies,
        clearSearchResults,
      }}>
      {props.children}
    </MovieContext.Provider>
  );
};

export default MovieState;
