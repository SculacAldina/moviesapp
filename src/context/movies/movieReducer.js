import {
  GET_POPULAR_MOVIES,
  GET_MOVIE_BY_ID,
  GET_MOVIE_CREDITS_BY_ID,
  ACTIVATE_SEARCH,
  GET_SEARCH_RESULTS,
  CLEAR_SEARCH_RESULTS,
} from '../types';

export default (state, action) => {
  switch (action.type) {
    case GET_POPULAR_MOVIES:
      return {
        ...state,
        movies: action.payload,
      };
    case GET_MOVIE_BY_ID:
      return {
        ...state,
        movie: action.payload,
      };
    case GET_MOVIE_CREDITS_BY_ID:
      return {
        ...state,
        movie_credits: action.payload,
      };
    case ACTIVATE_SEARCH:
      return {
        ...state,
        searchStatus: action.payload,
      };
    case GET_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: action.payload,
      };
    case CLEAR_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: [],
      };
    default:
      return state;
  }
};
