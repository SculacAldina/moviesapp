const colors = {
  main: '#0B253F',
  gray: '#EAEAEB',
  white: '#FFFFFF',
  black: '#000000',
};

export {colors};
