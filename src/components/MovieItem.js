import React from 'react';
import {StyleSheet, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

const MovieItem = props => {
  const {poster, onMovieSelected} = props;

  return (
    <TouchableOpacity onPress={onMovieSelected}>
      <Image
        style={styles.image}
        source={{
          uri: `https://image.tmdb.org/t/p/w500${poster}`,
        }}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 105,
    height: 154,
    borderRadius: 12,
  },
});

MovieItem.propTypes = {
  //poster: PropTypes.string.isRequired,
  onMovieSelected: PropTypes.func.isRequired,
};

export default MovieItem;
