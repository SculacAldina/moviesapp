import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import PropTypes from 'prop-types';

import {colors} from '../theme';

import MovieItem from './MovieItem';

const MovieList = props => {
  const {title, data, navigation} = props;

  const navToMovie = (screen, item) => {
    navigation.push(screen, {item});
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.list}>
        <FlatList
          columnWrapperStyle={styles.row}
          contentContainerStyle={{paddingBottom: 20}}
          showsVerticalScrollIndicator={false}
          scrollEnabled={true}
          data={data}
          keyExtractor={item => item.id}
          numColumns={3}
          renderItem={({item}) => {
            return (
              <View>
                <MovieItem
                  poster={item.poster_path}
                  onMovieSelected={() => navToMovie('Movie Details', item)}
                />
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  title: {
    fontSize: 20,
    lineHeight: 28,
    color: colors.main,
    fontWeight: '500',
    marginTop: 20,
    marginBottom: 20,
  },
  list: {
    flex: 1,
  },
  row: {
    flex: 1,
    justifyContent: 'space-between',
    marginBottom: 30,
  },
});

MovieList.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  navigation: PropTypes.object.isRequired,
};

export default MovieList;
