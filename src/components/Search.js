import React, {useState, useContext, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';

import MovieContext from '../context/movies/movieContext';

import {colors} from '../theme';

// Debounce Function
let timeOutId;
const debounce = (func, delay) => {
  return (...args) => {
    if (timeOutId) clearTimeout(timeOutId);
    timeOutId = setTimeout(() => {
      func.apply(null, args);
    }, delay);
  };
};

const Search = () => {
  const [input, setInput] = useState('');

  const {
    searchStatus,
    searchMovies,
    getPopularMovies,
    clearSearchResults,
    changeSearchStatus,
  } = useContext(MovieContext);

  const clearSearch = () => {
    setInput(''); // clears the search query
    clearSearchResults(); // clears the search results
  };

  const cancelSearch = () => {
    setInput(''); // clears the search query
    getPopularMovies();
    changeSearchStatus(false);
    Keyboard.dismiss();
    clearSearchResults(); // clears the search results
  };

  const handleChange = text => {
    setInput(text);
    if (text.length >= 3) debounceSearch(input);
  };

  const handleSearch = value => {
    searchMovies(value);
  };

  const debounceSearch = debounce(handleSearch, 500);

  return (
    <View style={styles.container}>
      <View
        style={{
          ...styles.inputContainer,
          width: searchStatus ? '80%' : '100%',
        }}>
        <Icon name={'search'} style={styles.icon} />
        <TextInput
          style={styles.input}
          onChangeText={handleChange}
          onSubmitEditing={() => searchMovies(input)}
          value={input}
          placeholder="Search"
          selectionColor={colors.main}
          autoCorrect={false}
          autoCapitalize="none"
          onPressIn={() => changeSearchStatus(true)}
        />
        {input ? (
          <TouchableOpacity
            onPress={() => clearSearch()}
            style={{alignSelf: 'center'}}>
            <Icon name={'close-a'} style={styles.iconClear} />
          </TouchableOpacity>
        ) : null}
      </View>
      {searchStatus ? (
        <TouchableOpacity onPress={() => cancelSearch()}>
          <Text style={styles.cancel}>Cancel</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  inputContainer: {
    height: 43,
    backgroundColor: colors.gray,
    borderRadius: 10,
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignSelf: 'center',
  },
  icon: {
    fontSize: 20,
    color: colors.main,
    alignSelf: 'center',
  },
  iconClear: {
    fontSize: 12,
    color: colors.main,
    marginRight: 10,
  },
  input: {
    width: '90%',
    height: '100%',
    fontSize: 16,
    lineHeight: 22.4,
    color: colors.main,
    paddingHorizontal: 11,
  },
  cancel: {
    fontSize: 16,
    lineHeight: 22.4,
    color: colors.main,
    marginLeft: 10,
  },
});

export default Search;
