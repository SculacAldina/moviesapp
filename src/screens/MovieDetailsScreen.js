import React, {useContext, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  ImageBackground,
  ScrollView,
} from 'react-native';

import PropTypes from 'prop-types';

import {colors} from '../theme';

import MovieContext from '../context/movies/movieContext';

// Get Movie Origin
const getOrigin = arr => {
  let origin = '';
  let mainCompany = {};
  if (arr.length > 0) {
    mainCompany = arr[0];
    if (mainCompany.origin_country) {
      origin = mainCompany.origin_country;
      return `(${origin})`;
    } else if (mainCompany.origin_country === '') {
      return origin;
    }
  }
};

// Get Movie Genres
const getGenres = arr => {
  let genres = '';
  for (let i = 0; i < arr.length; i++) {
    if (i < arr.length - 1) {
      genres += `${arr[i].name}, `;
    } else {
      genres += `${arr[i].name}`;
    }
  }
  return genres;
};

// Convert Minute to hh:mm
const getRunTime = minutes => {
  if (minutes <= 60) {
    return `${minutes} m`;
  } else {
    const m = minutes % 60;
    const h = (minutes - m) / 60;
    const hhmm = `${h}h ${m}m`;
    return hhmm;
  }
};

// Format Date
const formatDate = date => {
  let dataDate = new Date(date);
  const newDate =
    (dataDate.getMonth() > 8 // month
      ? dataDate.getMonth() + 1
      : '0' + (dataDate.getMonth() + 1)) +
    '/' +
    (dataDate.getDate() > 9 ? dataDate.getDate() : '0' + dataDate.getDate()) + // day
    '/' +
    dataDate.getFullYear(); // year
  return newDate;
};

// Show Credits
const showCredits = arr => {
  if (arr.length > 0) {
    let newArr = removeDuplicateId(arr);
    let creditsItems = [];
    for (let i = 0; i < 3; i++) {
      creditsItems.push(
        <View key={newArr[i].id}>
          <Text style={{...styles.credits, fontWeight: '500'}}>
            {newArr[i].name}
          </Text>
          <Text style={styles.credits}>{newArr[i].known_for_department}</Text>
        </View>,
      );
    }
    return creditsItems;
  }
};

// Remove Duplicates
const removeDuplicateId = arr => {
  const ids = arr.map(item => item.id);
  return arr.filter(({id}, index) => !ids.includes(id, index + 1));
};

const MovieDetailsScreen = ({route}) => {
  const {item} = route.params;

  const {movie, movie_credits, getMovieById, getMovieCreditsById} =
    useContext(MovieContext);

  useEffect(() => {
    getMovieById(item.id);
    getMovieCreditsById(item.id);
  }, []);

  return (
    <SafeAreaView style={styles.root}>
      <StatusBar backgroundColor={colors.main} barStyle="light-content" />
      <View>
        <ImageBackground
          style={styles.image}
          source={{
            uri: `https://image.tmdb.org/t/p/w500${movie.backdrop_path}`,
          }}>
          <View style={styles.movieInfo}>
            <Text style={styles.title}>{movie.original_title}</Text>
            <View style={{marginBottom: 10}}>
              <Text style={styles.text}>
                {movie.release_date ? formatDate(movie.release_date) : null}{' '}
                {movie.production_companies
                  ? getOrigin(movie.production_companies)
                  : null}
              </Text>
              <View style={{flexDirection: 'row', width: '90%'}}>
                <Text style={styles.text}>
                  {movie.genres ? getGenres(movie.genres) : null}
                </Text>
                <Text
                  style={{...styles.text, fontWeight: '600', marginLeft: 5}}>
                  {movie.runtime ? getRunTime(movie.runtime) : null}
                </Text>
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
      <View style={{flex: 1}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.detailContainer}>
            <Text style={styles.overviewTitle}>Overview</Text>
            <Text style={styles.overviewText}>{movie.overview}</Text>
            <View style={styles.rowCredits}>
              {movie_credits.cast ? showCredits(movie_credits.cast) : null}
            </View>
            <View style={styles.rowCredits}>
              {movie_credits.crew ? showCredits(movie_credits.crew) : null}
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white,
  },
  image: {
    width: '100%',
    height: 303,
  },
  movieInfo: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginHorizontal: 16,
  },
  title: {
    color: colors.white,
    fontSize: 24,
    lineHeight: 33.6,
    fontWeight: '500',
    marginBottom: 7,
  },
  text: {
    color: colors.white,
    fontSize: 14,
    lineHeight: 19.6,
  },
  overviewTitle: {
    fontSize: 20,
    lineHeight: 28,
    fontWeight: '500',
    color: colors.main,
    marginBottom: 10,
  },
  overviewText: {
    fontSize: 14,
    lineHeight: 19.6,
    color: colors.black,
    marginBottom: 22,
  },
  detailContainer: {
    marginHorizontal: 16,
    marginVertical: 20,
  },
  rowCredits: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 26,
  },
  credits: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 19.6,
    color: colors.black,
    marginBottom: 3,
    marginRight: 5,
  },
});

MovieDetailsScreen.propTypes = {
  route: PropTypes.object.isRequired,
};

export default MovieDetailsScreen;
