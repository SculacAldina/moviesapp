import React, {useContext} from 'react';
import {View, StyleSheet, StatusBar, SafeAreaView} from 'react-native';

import PropTypes from 'prop-types';

import Search from '../components/Search';
import MovieList from '../components/MovieList';

import MovieContext from '../context/movies/movieContext';

import {colors} from '../theme';

const HomeScreen = ({navigation}) => {
  const {movies, searchStatus, searchResults} = useContext(MovieContext);

  const showMovies = () => {
    if (!searchStatus) {
      return (
        <MovieList
          title="What's popular"
          data={movies}
          navigation={navigation}
        />
      );
    } else {
      if (searchResults) {
        if (searchResults.length === 0) {
          return null;
        } else {
          return (
            <MovieList
              title={`Showing ${searchResults.length} results`}
              data={searchResults}
              navigation={navigation}
            />
          );
        }
      }
    }
  };

  return (
    <SafeAreaView style={styles.root}>
      <StatusBar backgroundColor={colors.main} barStyle="light-content" />
      <Search />
      <View style={styles.container}>{showMovies()}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white,
  },
  container: {
    flex: 1,
  },
});

HomeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default HomeScreen;
